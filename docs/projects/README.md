---
title: projects

---



 <a href="https://elenavolpato.gitlab.io/dice-game/"> <img width="25%" style="float: right; margin-left: 1rem; box-shadow: 5px 5px 10px 2px #d3d3d3;" src="/assets/img/dice-game.png" alt="dice game image"> </a>

## dice game

This is one of the challenges from [Scrimba's](https://scrimba.com) Frontend Career Path. For this one, I watched the initial idea and goals and then decided to built this all on my own (using the css that was already set in the screencast). I built it using vanilla javascript and CSS.

  Go to [dice game](https://elenavolpato.gitlab.io/dice-game/)

  [Source Code](https://gitlab.com/elenavolpato/dice-game)

<br>

_______


 <a href="https://elenavolpato.gitlab.io/metric-imperial/"> <img width="20%" style="float: right; margin-left: 1rem; box-shadow: 5px 5px 10px 2px #d3d3d3;" src="/assets/img/metric-imperial.png" alt="metric imperial converter image"> </a>

## unit converter - metric ↔ imperial 

This a solo project from [Scrimba's](https://scrimba.com) Frontend Career Path. I had to get the design from a [Figma file](https://www.figma.com/file/cqtGul0V8RFXY4vTcIv1Kc/Unit-Conversion) and build it from scratch. It was really fun! I built it using vanilla javascript and CSS.

  Go to [unit converter](https://elenavolpato.gitlab.io/metric-imperial)

  [Source Code](https://gitlab.com/elenavolpato/metric-imperial)


<br>

_______



<a href="https://elenavolpato.gitlab.io/password-gen"> <img width="30%" style="float: right; margin-left: 1rem;  box-shadow: 5px 5px 10px 2px #d3d3d3;" src="/assets/img/password-gen.png" alt="password generator app image"> </a>

## password generator

This simple password generator is solo project of the Frontend Career Path from [Scrimba](https://scrimba.com). I also had to use a [Figma design file](https://www.figma.com/file/8gq5CBEg2IWxhOxTMmFTsE/Random-Password-Generator-(New-version)-(Copy)). You can select how many characters you want in your password and it will generate 2 different passwords. I built it using vanilla javascript and CSS.

  Go to [password generator](https://elenavolpato.gitlab.io/password-gen/)

  [Source Code](https://gitlab.com/elenavolpato/password-gen)
_______


<br>


<a href="https://meditate.elenavolpato.me/"> <img width="30%" style="float: right; margin-top:1rem; margin-left: 1rem; box-shadow: 5px 5px 10px 2px #d3d3d3;" src="/assets/img/Screenshot from 2021-04-28 13-31-37.png" alt="print from the meditation app"> </a>

## meditation timer app

I built this meditations timer app and wrote about it [here.](https://www.elenavolpato.me/2021/04/27/its-here/) This gave me a lot of confidence as front end developer and also a lot of fun! 

  Go to [meditation timer app](https://meditate.elenavolpato.me/)

  [Source Code](https://gitlab.com/elena_volpato/meditate)
_______



<a href="https://quiz.elenavolpato.me/#/"><img width="30%" style="float: right; box-shadow: 5px 5px 10px 2px #d3d3d3; margin-left: 1rem" src="/assets/img/Screenshot from 2021-04-28 13-42-09.png" alt="print from the quiz"> </a>

## quiz 

This project is a weekly web dev challenge from [scrimba](https://scrimba.com/) is a quiz maker. The challenge itself was to make a simple quiz, and validate answers. However, I took it a bit further and made 2 themes for you to choose from.

This was very interesting to do, because it was the first time a used router, which increased greatly the complexity of my projects.

  Go to [quiz](https://quiz.elenavolpato.me/#/)

  [Source Code](https://gitlab.com/elena_volpato/quiz)

_____

##### Checkout my [Gitlab page](https://gitlab.com/elena_volpato)
##### My resume [here](/assets/ResumeElenaVolpato.pdf) 


