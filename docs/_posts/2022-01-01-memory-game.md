---
title: The memory game
date: '2022-02-22'
tags: 
  - javascript
  - git
  - game
  - Italy
    
image: https://www.elenavolpato.me/assets/Castello-Rapallo.jpg
author: Elena Volpato
featuredimg: '/assets/Castello-Rapallo.jpg'
summary: 'Settling down and a new challenge'
---
### TL;DR: almost everything changed in the last 8 months :D

In June 2021, we left Sao Paulo to travel for three months to Berlin. After some weeks there, we decided we didn't want go back to Brazil and would try living in Berlin. We did some apartment hunting, sent a lot of emails, talked to some people, and after no possible rents showing up, we changed plans. And it turns out that now, the rent in Berlin is rising, and it is very hard to rent something there - it may take months or even a year. And, since I could be in Europe for only 3 months (tourist visa) I didn't have the time for that we decided to go to Italy and go for my Italian citizenship. We moved to a small town in Piemonte, I asked my family in Brazil to gather my documents, and in early September I had my _Permesso di Soggiorno_ (a residence permit) for another 6 months while I waited for my citizenship to be recognized. In December I got my Italian documents while I was moving to a city in Liguria, Rapallo - the city from the the post photo.

During this time I participated in a project with two other developers (you can read about it [here](https://www.elenavolpato.me/2021/09/09/first-project/)),I also have been studying programming as much as could and started studying Italian.

### What I've been up to

It got me thinking if I should keep on the developer track or change everything again. But then I went to my gitlab page and saw the commits in the last year, lots of them in march until may, then a lot in August. Still very little, I know. But I really enjoyed doing my website, then my meditation app, then the project in August and all the studying I've been doing. Solving problems as developer is something I really like.

Last week I took a personal challenge: make a memory game - something I was really good when I was a kid. And after 4 days working on it, it finally works as I first planned it. It was a really good practice and made me really think of algorithms, work with a small database and all that while thinking it could be something that could be bigger, something that if I want to add 1000 images, or change them, it will keep working.

### the memory game

Features:
* you can choose from 3 levels of difficulty: the easy one has 6 pairs of cards and the hardest one has 20.
* there are 20 images to shuffle from
* when you select the easy or normal mode it will not take the first ones, it will also shuffle the images
* the background color of each pair of cards is random from a previously defined list of colors
  
For now, it works the way I planned and I hope to add some other features to it soon.
**Click [here](https://elenavolpato.gitlab.io/memory-game/) to play!**



