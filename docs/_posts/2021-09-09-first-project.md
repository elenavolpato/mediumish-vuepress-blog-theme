---
title: Project with Sao Paulo City Hall
date: '2021-09-09'
tags: 
  - project
  - javascript
  - typescript
  - git
  - Vue3
  - Vite
  
image: https://www.elenavolpato.me/assets/page_CMP.png
author: Elena Volpato
featuredimg: '/assets/page_CMP.png'
summary: 'The first real project as a developer ☺'
---

The company of a friend signed a contract to build a website for a project called _Circuito da Memória Paulistana_, roughly translated to "Paulistana Memory Circuit". This project is part of a yearly initiative of the Culture Bureau of São Paulo named _Jornada do Patrimônio_(more about it [here](https://jornadadopatrimonio.prefeitura.sp.gov.br/2021/), in Portuguese). </br> This is the website: [Circuito de Memória Paulistana](https://circuitomemoriapaulistana.com.br) 

When he started, he knew the the deadline was tight: only one month to build a website from scratch, with a lot of content in it. As weeks passed by, two weeks before the time period was over, he asked me to join this project as a developer and Quality Assurance analyst.

The project involved some already familiar things frameworks like [Vite](https://vitejs.dev/), [Vue 3](https://v3.vuejs.org), and [tailwind](https://tailwindcss.com/), but also a new language: [Typescript](https://www.typescriptlang.org/). This was also the first project I worked with other people and a real client. So using `git` this time included other things. 

In my personal projects, such as the [meditation app](https://meditate.elenavolpato.me/), I committed and pushed everything to the `master` branch. This time, I had to create branches to everything I did and requested merges all the time. I also had to use `git pull` to update my local repository with other people's work. 

And, being my first collaborative project, I was very careful to commit every step of what I was doing, in case I made any mistakes and broke something -  and yeah, that happened and I could "rewind" it with `git log` and `git checkout paste-commit-code`. By the way, git is amazing! Thank you mister Linus Torvalds.

One of the main challenges was that we were building this website for mobile and desktop (with a 1920x1080 px resolution). And it took a while for us to understand why the client was saying the layout was "broken" or that she could "not see the changes". In fact, she had a notebook with a 1280x800 px screen. So everything was actually looking poorly made or broken on her screen. Then, we started adapting everything to that screen using tailwind media queries `lg:` and `xl:` to change the behavior between full HD and her smaller screen. Tailwind helped **A LOT** with that!

I learned a lot in this project and could get a good perspective on how it is to work in a team of developers. I'll keep on applying to jobs and project while I continue to study and work on my own projects.




